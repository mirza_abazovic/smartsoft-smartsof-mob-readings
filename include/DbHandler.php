<?php

/**
 * Class to handle all db operations
 * This class will have CRUD methods for database tables
 *
 * @author Ravi Tamada
 * @link URL Tutorial link
 */
class DbHandler {

    private $conn;
    private $enabledUser = 1;
    private $mobileUser = 2;

    function __construct() {
        require_once dirname(__FILE__) . '/DbConnect.php';
        // opening db connection
        $db = new DbConnect();
        $this->conn = $db->connect();
    }

    /* ------------- `users` table method ------------------ */

    /**
     * Creating new user
     * @param String $name User full name
     * @param String $email User login email id
     * @param String $password User login password
     */
    public function createUser($name, $email, $password) {
        require_once 'PassHash.php';
        $response = array();

        // First check if user already existed in db
        $userExists = $this->isUserExists($name);
        if ($userExists) {
            // User with same name already existed in the db
            return USER_ALREADY_EXISTED;
        } else {
            try { // Generating password hash
                $password_hash = PassHash::hash($password);

                // Generating API key
                $api_key = $this->generateApiKey();

                // insert query
                $stmt = $this->conn->prepare("INSERT INTO korisnici (korisnik, email, password_hash, api_key,aktivan,ovlast) values(?, ?, ?, ?,?,?)");
                $stmt->bind_param("ssssii", $name, $email, $password_hash, $api_key, $this->enabledUser, $this->mobileUser);
                //$stmt->bind_param("ssss", $name, $email, $password_hash, $api_key);

                $result = $stmt->execute();

                $stmt->close();

                // Check for successful insertion
                if ($result) {
                    // User successfully inserted
                    return USER_CREATED_SUCCESSFULLY;
                } else {
                    // Failed to create user
                    return USER_CREATE_FAILED;
                }
            } catch (Exception $ex) {
                return USER_CREATE_FAILED;
            }
        }

        return $response;
    }

    /**
     * Checking user login
     * @param String $name User name
     * @param String $password User login password
     * @return boolean User login status success/fail
     */
    public function checkLogin($name, $password) {
        // fetching user by email
        $stmt = $this->conn->prepare("SELECT password_hash FROM korisnici WHERE korisnik = ?");

        $stmt->bind_param("s", $name);

        $stmt->execute();

        $stmt->bind_result($password_hash);

        $stmt->store_result();

        if ($stmt->num_rows > 0) {
            // Found user with the email
            // Now verify the password

            $stmt->fetch();

            $stmt->close();

            if (PassHash::check_password($password_hash, $password)) {
                // User password is correct
                return TRUE;
            } else {
                // user password is incorrect
                return FALSE;
            }
        } else {
            $stmt->close();

            // user not existed with the email
            return FALSE;
        }
    }

    /**
     * Checking for duplicate user by email address
     * @param String $email email to check in db
     * @return boolean
     */
    private function isUserExists($name) {
        $stmt = $this->conn->prepare("SELECT id_korisnici from korisnici WHERE korisnik = ?");
        $stmt->bind_param("s", $name);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $result = $num_rows > 0;
        $stmt->close();
        return $result;
    }

    /**
     * Fetching user by email
     * @param String $email User email id
     */
    public function getUserByEmail($email) {
        $stmt = $this->conn->prepare("SELECT korisnik, email, api_key, aktivan, created_at FROM korisnici WHERE email = ?");
        $stmt->bind_param("s", $email);
        if ($stmt->execute()) {
            // $user = $stmt->get_result()->fetch_assoc();
            $stmt->bind_result($name, $email, $api_key, $status, $created_at);
            $stmt->fetch();
            $user = array();
            $user["name"] = $name;
            $user["email"] = $email;
            $user["api_key"] = $api_key;
            $user["status"] = $status;
            $user["created_at"] = $created_at;
            $stmt->close();
            return $user;
        } else {
            return NULL;
        }
    }

    /**
     * Fetching user by email
     * @param String $email User email id
     */
    public function getUserByName($name) {
        $stmt = $this->conn->prepare("SELECT korisnik, email, api_key, aktivan, created_at FROM korisnici WHERE korisnik = ?");
        $stmt->bind_param("s", $name);
        if ($stmt->execute()) {
            // $user = $stmt->get_result()->fetch_assoc();
            $stmt->bind_result($name, $email, $api_key, $status, $created_at);
            $stmt->fetch();
            $user = array();
            $user["name"] = $name;
            $user["email"] = $email;
            $user["api_key"] = $api_key;
            $user["status"] = $status;
            $user["created_at"] = $created_at;
            $stmt->close();
            return $user;
        } else {
            return NULL;
        }
    }

    /**
     * Fetching user api key
     * @param String $user_id user id primary key in user table
     */
    public function getApiKeyById($user_id) {
        $stmt = $this->conn->prepare("SELECT api_key FROM korisnici WHERE id_korisnici = ?");
        $stmt->bind_param("i", $user_id);
        if ($stmt->execute()) {
            // $api_key = $stmt->get_result()->fetch_assoc();
            // TODO
            $stmt->bind_result($api_key);
            $stmt->close();
            return $api_key;
        } else {
            return NULL;
        }
    }

    /**
     * Fetching user id by api key
     * @param String $api_key user api key
     */
    public function getUserId($api_key) {
        $stmt = $this->conn->prepare("SELECT id_korisnici FROM korisnici WHERE api_key = ?");
        $stmt->bind_param("s", $api_key);
        if ($stmt->execute()) {
            $stmt->bind_result($user_id);
            $stmt->fetch();
            // TODO
            // $user_id = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            return $user_id;
        } else {
            return NULL;
        }
    }

    /**
     * Validating user api key
     * If the api key is there in db, it is a valid key
     * @param String $api_key user api key
     * @return boolean
     */
    public function isValidApiKey($api_key) {
        $stmt = $this->conn->prepare("SELECT id_korisnici from korisnici WHERE api_key = ?");
        $stmt->bind_param("s", $api_key);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $stmt->close();
        return $num_rows > 0;
    }

    /**
     * Generating random Unique MD5 String for user Api key
     */
    private function generateApiKey() {
        return md5(uniqid(rand(), true));
    }

    /**
     * Fetching all user current readings
     * @param String $user_id id of the user
     */
    public function getCurrentReadingsForUser($user_id) {
        //SELECT CAST(`id_podaci` as char), CAST(`sifra_zone` as char), `naziv_zone`, cast(`sif_vodomjera` as char), `naziv_korisnika`, cast(`sifra_ulice` as char), `naziv_ulice`, cast(`serijski_broj` as char), cast(`nacin_ocit` as char), cast(`tip_ocit`  as char), cast(`broj_modula`  as char), `datu_zadnjeg_ocit`, cast(`zadnje_stanje_brojila`  as char), `status_ocitavanja`, cast(`longitude`  as char), cast(`latitude` as char), cast(`id_korisnik`  as char), cast(`trenutno_ocitanje` as char), `datum_ocitanja`, `stanje_brojila`, `datum_upisa_ocitanja` FROM `task_manager`.`podaci` 
        //$stmt = $this->conn->prepare("SELECT CAST(`id_podaci` as char), CAST(`sifra_zone` as char), `naziv_zone`, cast(`sif_vodomjera` as char), `naziv_korisnika`, cast(`sifra_ulice` as char), `naziv_ulice`, cast(`serijski_broj` as char), cast(`nacin_ocit` as char), cast(`tip_ocit`  as char), cast(`broj_modula`  as char), `datu_zadnjeg_ocit`, cast(`zadnje_stanje_brojila`  as char), `status_ocitavanja`, cast(`longitude`  as char), cast(`latitude` as char), cast(`id_korisnik`  as char), cast(`trenutno_ocitanje` as char), `datum_ocitanja`, `stanje_brojila`, `datum_upisa_ocitanja` FROM `podaci`  o WHERE o.id_korisnik = ? AND trenutno_ocitanje=1");

        $stmt = $this->conn->prepare("SELECT * FROM podaci WHERE id_korisnik = ? AND trenutno_ocitanje = 1");
        $stmt->bind_param("i", $user_id);
        $stmt->execute();
        $readings = $stmt->get_result();
        //$stmt->close();
        return $readings;
    }

    /**
     * Fetching all user readings on date
     * @param String $user_id id of the user
     */
    public function getReadingsOnDateForUser($user_id, $readings_date) {
        $stmt = $this->conn->prepare("SELECT CAST(o.id_podaci as CHAR(50)) as id_podaci, FROM podaci o, user_ositanje uo WHERE u.id = uo.task_id AND uo.user_id = ? AND DATUM = ?");
        $stmt->bind_param("ii", $user_id, $readings_date);
        $stmt->execute();
        $stmt->execute();
        $readings = $stmt->get_result();
        $stmt->close();
        return $readings;
    }

    /**
     * Function to update reading 
     * @param String $reading_id id of the user
     * @param String $reading_value value of the reading
     * @param String $reading_date date of the reading
     *      */
    public function updateReading($reading_id, $reading_value,$reading_date,$is_reset) {
        //$stmt = $this->conn->prepare("UPDATE podaci SET novo_stanje_brojila = ? datum_ocitanja= ? WHERE id_podaci=?");
        //$y = substr($reading_date,6,4);
        //$m = substr($reading_date,3,2);
        //$d = substr($reading_date,0,2);
        
        $date =str_replace("T"," ",$reading_date);
        $date =str_replace("Z","",$date);
     
        $this->conn->query('UPDATE podaci SET novo_stanje_brojila = '.$reading_value.', datum_ocitanja= \''.$date.'\', is_reset='.$is_reset.' WHERE trenutno_ocitanje=1 and id_podaci='.$reading_id);
        $result = $this->conn->affected_rows;       
        //$stmt = $this->conn->prepare("UPDATE podaci SET novo_stanje_brojila = ".$reading_value.", datum_ocitanja= '".$date."' WHERE id_podaci=".$reading_id);
        
        //$stmt->bind_param("isi", (int)$reading_value, $date,(int)$reading_id);       
        //$result = $stmt->execute();
        //if (false === $result) {
        //    die('execute() failed: ' . htmlspecialchars($stmt->error));
        //}
        //$stmt->close();
        return $result;
    }

}

?>
