CREATE DATABASE  IF NOT EXISTS `komunalno` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `komunalno`;
-- MySQL dump 10.13  Distrib 5.6.16, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: komunalno
-- ------------------------------------------------------
-- Server version	5.6.16

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `korisnici`
--

DROP TABLE IF EXISTS `korisnici`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `korisnici` (
  `id_korisnici` int(11) NOT NULL AUTO_INCREMENT,
  `korisnik` text,
  `lozinka` text,
  `ovlast` int(11) DEFAULT NULL,
  `ulice` text,
  `aktivan` bit(1) DEFAULT b'0',
  `email` varchar(100) DEFAULT NULL,
  `password_hash` varchar(250) DEFAULT NULL,
  `api_key` varchar(250) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_korisnici`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `log4php_log`
--

DROP TABLE IF EXISTS `log4php_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `log4php_log` (
  `timestamp` datetime DEFAULT NULL,
  `logger` varchar(256) DEFAULT NULL,
  `level` varchar(32) DEFAULT NULL,
  `message` varchar(20000) DEFAULT NULL,
  `thread` int(11) DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `line` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ovlasti`
--

DROP TABLE IF EXISTS `ovlasti`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ovlasti` (
  `id_ovlasti` int(11) NOT NULL AUTO_INCREMENT,
  `naziv` text,
  PRIMARY KEY (`id_ovlasti`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `podaci`
--

DROP TABLE IF EXISTS `podaci`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `podaci` (
  `id_podaci` int(11) NOT NULL AUTO_INCREMENT,
  `sifra_zone` int(11) DEFAULT NULL,
  `naziv_zone` text,
  `sif_vodomjera` int(11) DEFAULT NULL,
  `naziv_korisnika` text,
  `sifra_ulice` int(11) DEFAULT NULL,
  `naziv_ulice` text,
  `serijski_broj` int(11) DEFAULT NULL,
  `nacin_ocit` int(11) DEFAULT NULL,
  `tip_ocit` int(11) DEFAULT NULL,
  `broj_modula` int(11) DEFAULT NULL,
  `datu_zadnjeg_ocit` date DEFAULT NULL,
  `zadnje_stanje_brojila` int(11) DEFAULT NULL,
  `status_ocitavanja` enum('Brojilo je očitano','Onemogućen pristup objektu','Drugi razlozi') DEFAULT NULL,
  `longitude` tinytext,
  `latitude` tinytext,
  `id_korisnik` int(11) DEFAULT NULL,
  `trenutno_ocitanje` int(11) DEFAULT NULL,
  `datum_ocitanja` datetime DEFAULT NULL,
  `novo_stanje_brojila` int(11) DEFAULT NULL,
  `vrijeme_upisa_ocitanja` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_podaci`)
) ENGINE=InnoDB AUTO_INCREMENT=5167 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2014-09-29 12:17:03
