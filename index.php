<?php

require_once 'include/DbHandler.php';
require_once 'include/PassHash.php';
require 'libs/Slim/Slim.php';
include('libs/log4php/Logger.php');
 \Slim\Slim::registerAutoloader();
$app = new \Slim\Slim();

// User id from db - Global Variable
$user_id = NULL;
Logger::configure('config.xml');
// Fetch a logger, it will inherit settings from the root logger
$log = Logger::getLogger('myLogger');

/**
 * Adding Middle Layer to authenticate every request
 * Checking if the request has valid api key in the 'Authorization' header
 */
function authenticate(\Slim\Route $route) {
    //global $log;
    // Getting request headers
    if (!function_exists('apache_request_headers')) {
        function apache_request_headers() {
            $headers = array();
            foreach ($_SERVER as $key => $value) {
                if (substr($key, 0, 5) == 'HTTP_') {
                    $headers[str_replace(' ', '-', ucwords(str_replace('_', ' ', strtolower(substr($key, 5)))))] = $value;
                }
            }
            return $headers;
        }
    }
    $headers = apache_request_headers();
    $response = array();
    $app = \Slim\Slim::getInstance();
    if (isset($headers['Authorization'])) {
        $db = new DbHandler();
        //$log->debug("get the api key");
        // get the api key
        $api_key = $headers['Authorization'];
        if (!$db->isValidApiKey($api_key)) {
		// api key is not present in users table
                $response["error"] = true;
                $response["message"] = "Pristup odbijen";
                echoResponse(401, $response);
                $app->stop(); 
            } else {
                global $user_id;
                // get user primary key id
                $user_id = $db->getUserId($api_key);
            }
        }
     else {
        // api key is missing in header
        //$log->debug("api key is missing in header");
        $response["error"] = true;
        $response["message"] = "Pristup odbijen";
        echoResponse(400, $response);
        $app->stop();
    }
}

/**
 * User Registration
 * url - /register
 * method - POST
 * params - name, email, password
 */
$app->post('/register', function() use ($app) {
    // check for required params
    verifyRequiredParams(array('name', 'password'));

    $response = array();

    // reading post params
    $name = $app->request->post('name');
    $email = $app->request->post('email');
    $password = $app->request->post('password');

    // validating email address
    //validateEmail($email);

    $db = new DbHandler();
    $res = $db->createUser($name, $email, $password);

    if ($res == USER_CREATED_SUCCESSFULLY) {
        $response["error"] = false;
        $response["message"] = "Uspješna registracija korisnika";
    } else if ($res == USER_CREATE_FAILED) {
        $response["error"] = true;
        $response["message"] = "Desila se greška prilikom registracije";
    } else if ($res == USER_ALREADY_EXISTED) {
        $response["error"] = true;
        $response["message"] = "Korisničko ime je zauzeto";
    }
    // echo json response
    echoResponse(201, $response);
});
/**
 * User Login
 * url - /login
 * method - POST
 * params - email, password
 */
$app->post('/login', function() use ($app) {
    global $log;
    // check for required params
    verifyRequiredParams(array('user', 'password'));

    // reading post params
    $user = $app->request()->post('user');
    $password = $app->request()->post('password');
    $response = array();
    $db = new DbHandler();
    // check for correct username and password
    if ($db->checkLogin($user, $password)) {
        // get the user by name
        $user = $db->getUserByName($user);
        if ($user != NULL) {
            $response["error"] = false;
            $response['name'] = $user['name'];
            $response['email'] = $user['email'];
            $response['apiKey'] = $user['api_key'];
            $response['createdAt'] = $user['created_at'];
        } else {
            // unknown error occurred
            $response['error'] = true;
            $response['message'] = "Desila se greška. Molimo pokušajte kasnije";
        }
    } else {
        // user credentials are wrong
        $response['error'] = true;
        $response['message'] = 'Prijava neuspješna. Korisničko ime ili lozinka nisu ispravni';
    }
    echoResponse(200, $response);
});
/**
 * Listing all current readings of particual user
 * method GET
 * url /readings          
 */
$app->get('/readings', 'authenticate', function() {
    global $log;
    global $user_id;
    //$log->debug("Strating readings for user with id=" . $user_id);
    $response = array();
    $db = new DbHandler();
    // fetching all user tasks
    try {
        $result = $db->getCurrentReadingsForUser($user_id);
        //$log->debug("Results fetched with getCurrentReadingsForUser".count($result));
    } catch (Exception $ex) {
        $log->error("Exception in getCurrentReadingsForUser " . $ex);
    }
    $response["error"] = false;
    $response["readings"] = array();
    // looping through result and preparing tasks array
    try {
        while ($ocitanje = $result->fetch_assoc()) {
            $tmp = array();
            $tmp["id_podaci"] = $ocitanje["id_podaci"];
            $tmp["sifra_zone"] = $ocitanje["sifra_zone"];
            $tmp["naziv_zone"] = $ocitanje["naziv_zone"];
            $tmp["sif_vodomjera"] = $ocitanje["sif_vodomjera"];
            $tmp["naziv_korisnika"] = $ocitanje["naziv_korisnika"];
            $tmp["sifra_ulice"] = $ocitanje["sifra_ulice"];
            $tmp["naziv_ulice"] = $ocitanje["naziv_ulice"];
            $tmp["serijski_broj"] = $ocitanje["serijski_broj"];
            $tmp["nacin_ocit"] = $ocitanje["nacin_ocit"];
            $tmp["tip_ocit"] = $ocitanje["serijski_broj"];
            $tmp["broj_modula"] = $ocitanje["tip_ocit"];
            $tmp["datu_zadnjeg_ocit"] = $ocitanje["datu_zadnjeg_ocit"];
            $tmp["zadnje_stanje_brojila"] = $ocitanje["zadnje_stanje_brojila"];
            $tmp["status_ocitavanja"] = $ocitanje["status_ocitavanja"];
            $tmp["longitude"] = $ocitanje["longitude"];
            $tmp["latitude"] = $ocitanje["latitude"];
            $tmp["id_korisnik"] = $ocitanje["id_korisnik"];
            $tmp["trenutno_ocitanje"] = $ocitanje["trenutno_ocitanje"];
            $tmp["datum_ocitanja"] = $ocitanje["datum_ocitanja"];
            $tmp["novo_stanje_brojila"] = $ocitanje["novo_stanje_brojila"];
            $tmp["vrijeme_upisa_ocitanja"] = $ocitanje["vrijeme_upisa_ocitanja"];
            array_push($response["readings"], $tmp);
        }
        //$log->debug("Results mapped to response");
    } catch (Exception $ex) {
        $log->error("Exception in mapping results to response " . $ex);
    }
    echoResponse(200, $response);
});

/**
 * Listing readings for date of particual user
 * method GET
 * url /readings/:date
 * Will return 404 if the user has no readings on that day
 */
$app->get('/readings/:date', 'authenticate', function($date) {
    global $user_id;
    $response = array();
    $db = new DbHandler();
    // fetch readings
    $result = $db->getReadingsOnDateForUser($date, $user_id);
    if ($result != NULL) {
        $response["error"] = false;
        $response["id"] = $result["id"];
        //$response["task"] = $result["task"];
        //$response["status"] = $result["status"];
        //$response["createdAt"] = $result["created_at"];
        echoResponse(200, $response);
    } else {
        $response["error"] = true;
        $response["message"] = "Nema očitanja za taj datum";
        echoResponse(404, $response);
    }
});

/**
 * Creating new readings in db
 * method POST
 * params - readings_data json data with all readings
 * url - /readings/
 */
$app->post('/readings', 'authenticate', function() use ($app) {
    // check for required params
    //verifyRequiredParams(array('readings'));
    $update_num = 0;
    $send_num = 0;
    $response = array();
    $requestBody = $app->request()->getBody(); 
    //$requestBodyDecoded  = urldecode($requestBody);
    $readings_data = json_decode($requestBody);  
    global $user_id;
    global $log;
    
    //$log->debug("User id=".$user_id.+" Sending updates: ".$requestBody);
    $errorMessage="";
    $db = new DbHandler();
    foreach ($readings_data->readings as $reading) {
        $send_num++;
        if($reading->reading_date!="null" && $reading->reading_value!="null" && $reading->reading_value!="") {
            $ocitanje_id = $db->updateReading($reading->reading_id,$reading->reading_value, $reading->reading_date,$reading->reading_is_reset);
            if($ocitanje_id==1){
                $update_num++;
            }
            if($ocitanje_id==-1){
                $errorMessage = $errorMessage." Nije uspješno uneseno očitanje id=".$reading->reading_id." vrijednost=".$reading->reading_value." datum=".$reading->reading_date;
            }
        }
      }
//        if($update_num==0){
//            $response["error"] = true;
//            $response["message"] = "Došlo je do greške. Očitanja nisu učitana";
//            echoResponse(200, $response);
//        }
        
    $response["message"] = "Broj poslanih očitanja: ".$send_num." . Broj izmjena na serveru: ".$update_num;
    $response["send_num"] = $send_num;
    $response["update_num"] = $update_num;
    $response["errormessage"] = $errorMessage;
    echoResponse(201, $response);
});

/**
 * Verifying required params posted or not
 */
function verifyRequiredParams($required_fields) {
    $error = false;
    $error_fields = "";
    $request_params = array();
    $request_params = $_REQUEST;
    // Handling PUT request params
    if ($_SERVER['REQUEST_METHOD'] == 'PUT') {
        $app = \Slim\Slim::getInstance();
        parse_str($app->request()->getBody(), $request_params);
    }
    foreach ($required_fields as $field) {
        if (!isset($request_params[$field]) || strlen(trim($request_params[$field])) <= 0) {
            $error = true;
            $error_fields .= $field . ', ';
        }
    }
    if ($error) {
        // Required field(s) are missing or empty
        // echo error json and stop the app
        $response = array();
        $app = \Slim\Slim::getInstance();
        $response["error"] = true;
        $response["message"] = 'Obavezno polje/a ' . substr($error_fields, 0, -2) . ' nedostaje';
        echoResponse(400, $response);
        $app->stop();
    }
}

/**
 * Validating email address
 */
function validateEmail($email) {
    $app = \Slim\Slim::getInstance();
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $response["error"] = true;
        $response["message"] = 'Email adresa nije ispravna';
        echoResponse(400, $response);
        $app->stop();
    }
}

/**
 * Echoing json response to client
 * @param String $status_code Http response code
 * @param Int $response Json response
 */
function echoResponse($status_code, $response) {
    $app = \Slim\Slim::getInstance();
    // Http response code
    $app->status($status_code);
    // setting response content type to json
    $app->contentType('application/json');
    echo json_encode($response);
}

$app->run();
?>