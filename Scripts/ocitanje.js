var version = "1.0";
var apiKey = null;
var currentUrl = window.location.protocol + '//' + window.location.host
var pathName = 'mob';
var loginUrl = currentUrl + '/' + pathName + '/index.php/login';
var registerUrl = currentUrl + '/' + pathName + '/index.php/register';
var currentReadingsUrl = currentUrl + '/' + pathName + '/index.php/readings';
var saveReadingsUrl = currentUrl + '/' + pathName + '/index.php/readings';
var total = 0;
var numberOfRecordsInLocalSql = 0;
var createStatement = "CREATE TABLE IF NOT EXISTS ocitanje (id INTEGER PRIMARY KEY AUTOINCREMENT, server_id TEXT,rbr TEXT, sifra TEXT,naziv TEXT,ulica TEXT,prethodno_ocitanje TEXT, novo_ocitanje INTEGER ,datum_ocitanja TEXT,longitude TEXT,latitude TEXT,is_reset INTEGER)";
var selectAllStatement = "SELECT * FROM ocitanje";
var countStatement = "SELECT COUNT(*) FROM ocitanje";
var insertStatement = "INSERT INTO ocitanje (server_id ,rbr, sifra ,naziv ,ulica ,prethodno_ocitanje,longitude ,latitude) VALUES (?,?,?,?,?,?,?,?)";
var updateOcitanjeStatement = "UPDATE ocitanje SET novo_ocitanje=?, datum_ocitanja = ?, is_reset=? WHERE id=?";
var updateResetStatement = "UPDATE ocitanje SET is_reset=? WHERE id=?";
var deleteAllStatement = "DELETE FROM ocitanje";
var dropStatement = "DROP TABLE ocitanje";
var selectReadingsForUpdateStatement = "SELECT server_id,novo_ocitanje,datum_ocitanja,is_reset FROM ocitanje WHERE (novo_ocitanje IS NOT NULL OR novo_ocitanje>0)AND datum_ocitanja IS NOT NULL ";
var db = openDatabase("Ocitanje", "1.0", "Ocitanje", 200000);  // Open SQLite Database
var dataset;
var DataType;
var username;

function setPocetnaFooter() {
                checkIfDataInTableExists(function (num) {
                    if (num === 0) {
                        $("#pocetnafooter").text('Nema podataka u lokalnoj bazi');
                    }
                    else {
                        $("#pocetnafooter").text(num + ' zapisa u lokalnoj bazi');
                    }
                });
            }
function getCurrentRedings(cb) {
    $.ajax({
        type: "GET",
        url: currentReadingsUrl,
        success: function (data) {
            if (data.error) {
                 $("#info").html('Greska' + data.message);
                //alert('Greska' + data.message);
            }
            else {
                //alert('Uspjesno' + JSON.stringify(data));
                if (data.readings.length === 0) {
                    $.mobile.loading("hide");
                    deleteAll();
                    showRecords();
                    //alert("Nema podataka");
                    $("#info").html('Nema podataka');
                } else {
                    cb(data);
                }
            }
        },
        error: function (data) {
             $.mobile.loading("hide");      
            //alert('Greška: ' + data.responseContent);
            if (!data.responseJSON) {
                //alert('Došlo je do greške prilikom preuzimanja ocitanja sa' + currentReadingsUrl);
                 $("#info").html('Došlo je do greške prilikom preuzimanja ocitanja sa' + currentReadingsUrl);
            }
            else {
                //alert('Greška: ' + data.responseJSON.message);
                 $("#info").html('Greška: ' + data.responseJSON.message);
            }
        }
    });

}
function initDatabase() {
    try {
       //if (!window.indexedDB) {
            //window.alert("Your browser doesn't support a stable version of IndexedDB. Such and such feature will not be available.");
       //}
        if (!window.openDatabase) {
            alert('Aplikacija nije podržana u ovom browseru. Odaberite Chrome ili Safari.');
        }
        else {
            createTable();  // If supported then call Function for create table in SQLite
        }
    }
    catch (e) {
        if (e == 2) {
            console.log("Invalid database version.");
        }
        else {
            console.log("Unknown error " + e + ".");
        }
        return;
    }
}
function createTable() {
    db.transaction(function (tx) {
        tx.executeSql(createStatement, [], onCreateTableSuccess, onError);
    });
}
function onCreateTableSuccess() {
}
function deleteAll(cb) {
    //alert('brisem sve');
    db.transaction(function (tx) {
        tx.executeSql(deleteAllStatement, [], cb, onError);
    });
}
function checkIfDataInTableExists(cb) {
    //db.transaction(function (tx) { tx.executeSql(selectAllStatement, [], null, onError); }); 
    db.transaction(function (tx) {
        tx.executeSql(selectAllStatement, [],
                function (tx, results) {
                    var len = results.rows.length;
                    cb(len);
                }, onError);
    });
}
function insertOcitanje(rbr, reading, cb) {
    db.transaction(function (tx) {
        tx.executeSql(insertStatement, [reading.id_podaci, rbr, reading.sif_vodomjera, reading.naziv_korisnika, reading.naziv_ulice, reading.zadnje_stanje_brojila, reading.longitude, reading.latitude], onInsertOcitanjeSuccess(rbr), onError);
    });
}
var populateData = function populateData(data) {
    total = data.readings.length;
    for (var i = 0; i < total; i++) {
        insertOcitanje(i + 1, data.readings[i]);
    }
}
function onSuccess() {
    //console.log("transakcija OK.");
    //alert('transakcija OK');
}
function onInsertOcitanjeSuccess(rbr) {
    var text = 'Prenos ' + rbr + ' od ' + total;
    $('#info').html(text);
    $.mobile.loading("show", {text: "Molimo sačekajte", textVisible: true});
    if (rbr === total) {
        $('#info').html('');
        $.mobile.loading("hide");
        showRecords();
    }
}
function createRow(reading) {
    var htmlRow = '<tr id="' + reading['id'] + '">'
            + '<td>Redni br.:&nbsp;<strong>' + reading['rbr'].substring(0, reading['rbr'].indexOf('.')) + '</strong></td>'
            //+ '<td>' + reading['sifra'] + '</td>'
            + '<td>Šifra:&nbsp;<strong>' + reading['sifra'].substring(0, reading['sifra'].indexOf('.')) + '</strong></td>'
            + '<td>Korisnik:&nbsp;<strong>' + reading['naziv'] + '</strong></td>'
            + '<td>Ulica:&nbsp;<strong>' + reading['ulica'] + '</strong></td>'
            //+ '<td>' + reading['ulica_broj'] + '</td>'
            + '<td class="alignright" id="prethodno_' + reading['id'] + '"><strong>'
            + reading['prethodno_ocitanje']
            //+ reading['prethodno_ocitanje'].substring(1,reading['prethodno_ocitanje'].length-2)
            + '</strong></td>'
            + '<td width="5"><input type="number" class="ocitanje" name="ocitanje_'
            + reading['id'] + '" id="ocitanje_' + reading['id']
            + '" value="' + reading['novo_ocitanje']
            + '" onblur="updateOcitanje(' + reading['id'] + ',ocitanje_' + reading['id'] + ');"/>'
            + '&nbsp;<span class="gray" id="potrosnja_' + reading['id'] + '">'+ getPotrosnja(reading['id'],reading['prethodno_ocitanje'], reading['novo_ocitanje'])+'</span></td>'
            + '<td><a href="#" id="reset_link_' + reading['id'] + '" onclick="toogleVisible(reset_hiden_' + reading['id'] +');">Reset</a><p id="reset_hiden_' + reading['id'] + '" class="display-none"><input type="checkbox" onchange=updateReset(' + reading['id'] + ') name="reset_' + reading['id'] + '" value="reset" id="reset_' + reading['id'] + '">Reset brojila<br></p>'            
            +'<hr></td>'          
            + '</tr>';
    return htmlRow;
}
function toogleVisible(id){
    $("#"+id.id).toggleClass("display-none");
}
function updateOcitanje(id, elementId) {
    var ocitanjeNum = Number(elementId.value);
    var prethodnoNum = Number($("#prethodno_" + id).text());
    var resetNum = Number($("#reset_" + id).is(':checked'));
    var potrosnja = ocitanjeNum - prethodnoNum;
    $("#potrosnja_" + id).html(potrosnja);
    if (ocitanjeNum > 0 && ocitanjeNum < prethodnoNum) {
        $.notify("Očitanje manje od " + prethodnoNum + " prethodnog!?", "error");
        //elementId.focus();
        //$("#potrosnja_" + id).css("color","red");
    }
    else {
         //$("#potrosnja_" + id).css("color","green");
          }
        db.transaction(function (tx) {
            tx.executeSql(updateOcitanjeStatement, [elementId.value, new Date(new Date().getTime() + (-new Date().getTimezoneOffset()*60000)).toISOString(),resetNum, Number(id)], null /*function(){alert('OK  ');}*/, function () {
                alert('Greška prilikom upisa u lokalnu bazu');
            });
        });      
}
function updateReset(id) {  
    var resetNum = Number($("#reset_" + id).is(':checked'));
        db.transaction(function (tx) {
            tx.executeSql(updateResetStatement, [resetNum, Number(id)], null , function () {
                alert('Greška prilikom upisa u lokalnu bazu');
            });
        });      
}
function getPotrosnja(id,prethodno,trenutno){
 
    var razlika = Number(trenutno) - Number(prethodno);
    if(razlika === "NaN" ||typeof(razlika )=== "undefined"){
        
    }
    else {
        //if(razlika<0) 
        //$(this).css('color', 'red'); 
           
        //else   
          //$(this).css('color', 'green'); 
        return razlika;
    };
}
function onError(tx, error) {
    alert(error.message);
    // error.code is a numeric error code
    alert('Error ' + error.message + ' (Code ' + error.code + ')');
    return true;
//var we_think_this_error_is_fatal = true;
    //if (we_think_this_error_is_fatal) return true;
    //return false;
}
function showRecords() {
    $("#tablebody").html('')
    db.transaction(function (tx) {
        tx.executeSql(selectAllStatement, [], function (tx, result) {
            dataset = result.rows;
            for (var i = 0, item = null; i < dataset.length; i++) {
                item = dataset.item(i);
                var row = createRow(item);
                $("#tablebody").append(row);
            }

        });

    });
}
function searchTable(inputVal) {
    var table = $('#myTable');
    table.find('tr').each(function (index, row) {
        var allCells = $(row).find('td');
        if (allCells.length > 0) {
            var found = false;
            allCells.each(function (index, td) {
                var regExp = new RegExp(inputVal, 'i');
                if (regExp.test($(td).text())) {
                    found = true;
                    return false;
                }
            });
            if (found == true)
                $(row).show();
            else
                $(row).hide();
        }
    });
}
function errorHandler() {

}
function nullHandler() {
}
function sendData() {
    $("#message").html('');
    $("#error-message").html('');
    newJson = '{ "readings": [';
    db.transaction(function (transaction) {
        transaction.executeSql(selectReadingsForUpdateStatement, [],
                function (transaction, result) {
                    if (result != null && result.rows != null) {
                        for (var k = 0; k < result.rows.length; k++) {
                            var row = result.rows.item(k);
                            if (k > 0)
                                newJson += ',';
                            newJson += '{ "reading_id":"' + row.server_id.substring(0, row.server_id.indexOf('.')) + '", "reading_value":"' + row.novo_ocitanje + '", "reading_date":"' + row.datum_ocitanja + '", "reading_is_reset":"' + row.is_reset + '"}';
                        }
                        jsonall = newJson + ']}';
                        //alert(jsonall); //shows me a correct JSON as string
                        jsonobjoff = $.parseJSON(jsonall);
                        $.ajax({
                            type: "POST",
                            url: saveReadingsUrl,
                            //data: jsonobjoff,
                            data: jsonall,
                            processData: false,
                            success: function (data) {
                                if (data.error) {
                                    //alert('Greska' + data.message);
                                    $("#error-message").html('Greška: ' + data.message);
                                }
                                else {
                                    //alert('Uspjesno' + JSON.stringify(data));
                                    $("#message").html(data.message);
                                    $("#error-message").html(data.errormessage);
                                    $.mobile.changePage("#info_slanje_dialog");
                                }
                            },
                            error: function (data) {
                                //alert('Greška: ' + data.responseContent);
                                if (!data.responseJSON) {
                                      $("#error-message").html('Došlo je do greške prilikom slanja podataka na ' + saveReadingsUrl + ' Poruka: ' + data.responseText);
                                    //alert('Došlo je do greške prilikom slanja podataka na ' + saveReadingsUrl + ' Poruka: ' + data.responseText);
                                }
                                else {
                                    //('Greška: ' + data.responseJSON.message);
                                    $("#error-message").html('Greška: ' + data.responseJSON.message);
                                }
                            }
                        });
                    }
                }, errorHandler);
    }, errorHandler, nullHandler);
}

